/* 
  jRACI project
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.objects;

import ch.consulare.jraci.RaciException;

/**
 * A single history record for a RACI
 *
 * Note that it doesn't compare the PrimaryUserId or the deleted flag, as this is intended only to
 * to provide the difference in between two RACIs. Restricting to PrimaryUserId or deleted
 * would limit the usability as some use cases may require to diff the RACI of a same object
 * from two different users.
 *
 * Once computed the object becomes immutable and Exceptions will be thrown if it is attempted 
 * to modify it.
*/
public class RaciDiff implements java.io.Serializable {	
	private static final long serialVersionUID = 2234951983785512352L;
	
	private RaciState r = RaciState.NOTSET;
	private RaciState a = RaciState.NOTSET;
	private RaciState c = RaciState.NOTSET;
	private RaciState i = RaciState.NOTSET;	
	
	private boolean originSet = false; 	// track if origin has been set
	private boolean	computed = false;	// track if computation has been done

	/**
	 * Default constructor
	 */	
	public RaciDiff() { /* zilch */ }

	/**
	 * Generates a diff from two RACI
	 *
	 * @throws IllegalArgumentException if any of the two are null
	*/
	public RaciDiff(final RACI origin, final RACI modified) throws RaciException, IllegalStateException, IllegalArgumentException {
		if (origin == null || modified == null)
			throw new IllegalArgumentException("The origin and modified RACI cannot be null");
		
		setOriginalRaci(origin);
		computeDiff(modified);
	}
	
	/**
	 * Sets the original RACI
	 *
	 * @throws IllegalArgumentException if the origin is null
	 * @param origin the baseline RACI, cannot be null
	 */
	public void setOriginalRaci(final RACI origin) throws IllegalArgumentException {
		if (origin == null)
			throw new IllegalArgumentException("Cannot diff null RACI");
						
		r = origin.isR() ? RaciState.SET : RaciState.NOTSET;
		a = origin.isA() ? RaciState.SET : RaciState.NOTSET;
		c = origin.isC() ? RaciState.SET : RaciState.NOTSET;
		i = origin.isI() ? RaciState.SET : RaciState.NOTSET;
		
		// ok.
		originSet = true;
    }

	/**
	 * one may compute a diff with a non-set raci... fix...
	 */
	public void computeDiff(final RACI modified) throws RaciException, IllegalStateException, IllegalArgumentException {
		if (modified == null)
			throw new IllegalArgumentException("Cannot diff RACI against a null one");
		
		if (! originSet)
			throw new IllegalStateException("Cannot compute a diff without an origin RACI being set");
		
		if (isComputed())
			throw new RaciException("The diff has already been computed");
						
		// R
		if (modified.isR() && r == RaciState.NOTSET) {
			r = RaciState.ADDED;
		} else if (!modified.isR() && r == RaciState.SET) {
			r = RaciState.REMOVED;
		}
		
		// A
		if (modified.isA() && a == RaciState.NOTSET) {
			a = RaciState.ADDED;
		} else if (!modified.isA() && a == RaciState.SET) {
			a = RaciState.REMOVED;
		}
		
		// C
		if (modified.isC() && c == RaciState.NOTSET) {
			c = RaciState.ADDED;
		} else if (!modified.isC() && c == RaciState.SET) {
			c = RaciState.REMOVED;
		}
		
		// I
		if (modified.isI() && i == RaciState.NOTSET) {
			i = RaciState.ADDED;
		} else if (!modified.isI() && i == RaciState.SET) {
			i = RaciState.REMOVED;
		}	
		
		// lock results
		computed = true;	
    }
    
    /** Returns true if computed */
    public boolean isComputed() { return computed; }
    
	/**
	 * Returns true if the history record contains a change - it should!
	 *
	 * @throws IllegalStateException if the computation hasn't yet occured
	 */
	public boolean hasChanged() throws IllegalStateException {
		if (!isComputed())
			throw new IllegalStateException("The RACI diff has not been computed");
		
		return (r == RaciState.ADDED || r == RaciState.REMOVED 
			|| a == RaciState.ADDED || a == RaciState.REMOVED 
			|| c == RaciState.ADDED || c == RaciState.REMOVED 
			|| i == RaciState.ADDED || i == RaciState.REMOVED); 
    }

	/** Returns the number of changes */
	public int getNumberOfChanges() throws IllegalStateException {
		if (!isComputed())
			throw new IllegalStateException("The RACI diff has not been computed");

		int count = 0;
		if (r == RaciState.ADDED || r == RaciState.REMOVED) ++count;  
		if (a == RaciState.ADDED || a == RaciState.REMOVED) ++count; 
		if (c == RaciState.ADDED || c == RaciState.REMOVED) ++count;
		if (i == RaciState.ADDED || i == RaciState.REMOVED) ++count;
		
		return count;
	}

	public RaciState getR() { return r; }
	
	public RaciState getA() { return a; }
	
	public RaciState getC() { return c; }
	
	public RaciState getI() { return i; }

	/**
	* Returns a string with the changes
	*/
	@Override
	public String toString() {
		return "R ["+r.getSymbol()+"], A ["+a.getSymbol()+"], C ["+c.getSymbol()+"], I ["+i.getSymbol()+"]";		
	}
}