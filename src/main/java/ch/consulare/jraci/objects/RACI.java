/* 
  jRACI projects, https://sourceforge.net/p/jraci/
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.objects;

import java.util.UUID;

import ch.consulare.jraci.IRaciObjectType;

/**
 * Objects to hold a line of a RACI matrix
 */
public class RACI implements java.io.Serializable, Comparable<RACI> {
    static final long serialVersionUID = 4113195983002863429L;
    
    private final UUID raciUUID = UUID.randomUUID();			// To uniquely identify the RACI
    
	private	RaciUnit unitR = RaciUnit.R_FALSE;					// all set to FALSE to start
    private	RaciUnit unitA = RaciUnit.A_FALSE;
    private	RaciUnit unitC = RaciUnit.C_FALSE;
    private	RaciUnit unitI = RaciUnit.I_FALSE;
	
	private RaciHistory history = null;							// internal change records
	
    private	String			primaryUserId		= null;			// The user id corresponding to this RACI line
	private boolean			deleted				= false;		// flag to mark the RACI as deleted or to be deleted
		
	/** Default constructor */
	public RACI() { /* nada */ }
	
	public RACI(final String _userId, final boolean _r, final boolean _a, final boolean _c, final boolean _i) {
		unitR = RaciUnit.getRaciUnit(R_A_C_I.R, _r);
		unitA = RaciUnit.getRaciUnit(R_A_C_I.A, _a);
		unitC = RaciUnit.getRaciUnit(R_A_C_I.C, _c);
		unitI = RaciUnit.getRaciUnit(R_A_C_I.I, _i);
		
		primaryUserId	= _userId;
	}
	
	public RACI(final RaciUnit _unit, final String _userId) {
		setUnit(_unit);
		primaryUserId	= _userId;
	}
		
	
	/** Clone but only the ranks and primaryUserId */
    public RACI(final RACI orig) {
		unitR = orig.getUnitR();
		unitA = orig.getUnitA();
		unitC = orig.getUnitC();
		unitI = orig.getUnitI();
		
		primaryUserId	= orig.getPrimaryUserId();
	}
    
    /** Returns the UUID that uniquely identifies this object instance */
    public UUID getRaciUUID() { return raciUUID; }
    
	/** Sets one of the RACI unit (R, A, C, I) */
	private final void setUnit(RaciUnit _unit) {	
		if (_unit == RaciUnit.NULL)
			return;
		
		switch (_unit.getUnit()) {
			case R	: unitR = _unit; break;
			case A	: unitA = _unit; break;
			case C	: unitC = _unit; break;
			case I	: unitI = _unit; break;
			default : break;
		}			
	}
	
	// Setter & Getter
	public final void setUnitR(RaciUnit unit) throws Exception { 
		if (unit.isNull())	// for soap deserializer if none set, flash doesn't support enumeration
			return;
		
		if (! unit.isR())
			throw new IllegalArgumentException("The R_A_C_I of the unit ("+unit+") must be R");
	
		unitR = unit; 
	}
	
	public final void setUnitA(RaciUnit unit) throws Exception {
		if (unit.isNull())
			return;

		if (! unit.isA())
			throw new IllegalArgumentException("The R_A_C_I of the unit ("+unit+") must be A");

		unitA = unit; 
	}
	
	public final void setUnitC(RaciUnit unit) throws Exception { 
		if (unit.isNull())
			return;

		if (! unit.isC())
			throw new IllegalArgumentException("The R_A_C_I of the unit ("+unit+") must be C");

		unitC = unit; 
	}
	
	public final void setUnitI(RaciUnit unit) throws Exception { 
		if (unit.isNull())
			return;

		if (! unit.isI())
			throw new IllegalArgumentException("The R_A_C_I of the unit ("+unit+") must be I");

		unitI = unit; 
	}

	public final RaciUnit getUnitR() { return unitR; }
	public final RaciUnit getUnitA() { return unitA; }
	public final RaciUnit getUnitC() { return unitC; }
	public final RaciUnit getUnitI() { return unitI; }

    public final boolean isR() { return unitR.isState(); }
    public final boolean isA() { return unitA.isState(); }
	public final boolean isC() { return unitC.isState(); }
    public final boolean isI() { return unitI.isState(); }
	public final boolean isAny() { 
		return (unitR.isState() || unitA.isState() || unitC.isState() || unitI.isState()); 
	}
	
    public final void setR(boolean _r) { unitR = RaciUnit.getRaciUnit(R_A_C_I.R, _r); }
    public final void setA(boolean _a) { unitA = RaciUnit.getRaciUnit(R_A_C_I.A, _a); }
	public final void setC(boolean _c) { unitC = RaciUnit.getRaciUnit(R_A_C_I.C, _c); }
    public final void setI(boolean _i) { unitI = RaciUnit.getRaciUnit(R_A_C_I.I, _i); }
    public final void makeRA(boolean _ra) { setA(_ra); setR(_ra); }

	public final void setPrimaryUserId(String _userId) { primaryUserId = _userId; }
    public final String getPrimaryUserId() { return primaryUserId; }
    
	public final boolean isDeleted() { return deleted; }
	public final void setDeleted(boolean _deleted) { deleted = _deleted; }
		
	/** 
	 * Returns true if the RACI contains the equivalent RaciUnit 
	 */
	public final boolean contains(final RaciUnit unit) {
		if (unit == null) return false;
		
		switch (unit.getUnit()) {
			case R	: return (unit.equals(unitR));
			case A	: return (unit.equals(unitA));
			case C	: return (unit.equals(unitC));
			case I	: return (unit.equals(unitI));
			default : break;
		}
		
		// all false
		return false;
	}
	
	/**
	 * Returns the best rank or the NULL rank.
	 */
	public final int getBestRank() {
		if (unitA == RaciUnit.A) return R_A_C_I.A.getRank();
		if (unitR == RaciUnit.R) return R_A_C_I.R.getRank();
		if (unitC == RaciUnit.C) return R_A_C_I.C.getRank();
		if (unitI == RaciUnit.I) return R_A_C_I.I.getRank();

		return R_A_C_I.NULL.getRank();
	}
			 
	/** 
	 * Returns the highest unit or null 
	 */
	public final RaciUnit getBestUnit() {
		if (unitA == RaciUnit.A) return RaciUnit.A;
		if (unitR == RaciUnit.R) return RaciUnit.R;
		if (unitC == RaciUnit.C) return RaciUnit.C;
		if (unitI == RaciUnit.I) return RaciUnit.I;
		
		return null;
	}
	
	/** Returns true if in the RACI the R OR A is set */
    public final boolean isRA() { return ( isR() || isA()); }
	
	/** Returns true if in the RACI the C or I is set */
	public final boolean isCI() { return (isC() || isI()); }
	
	/** Returns true if in the RACI the C or I is set */
	public final boolean isRAC() { return (isRA() || isC()); }

	/** Returns true if in the RACI the R AND A are both set */
	public final boolean isNotStrict() { return ( (isR() && isA() ? true : false)); }
	
	/** Returns true if the RACI is ok :
	 * C or I set or A or/and R.
	 * Assume that an empty RACI is valid.
	 */
	public final boolean isValid() {
		return (isRA() && isCI() ? false : true);
	}
			
	/** Reset all the RACI Unit to FALSE levels */
    public final void reset() {
		unitA = RaciUnit.A_FALSE; 
		unitR = RaciUnit.R_FALSE; 
		unitC = RaciUnit.C_FALSE;
		unitI = RaciUnit.I_FALSE;
    }
    
	/**
	 * Returns a string representation, only the letters of
	 * RACI set (ie. RA, C, etc).
	 */
    public final String getRaci(){
		final StringBuffer ret = new StringBuffer();
		ret.append(unitR.getString());
		ret.append(unitA.getString());
		ret.append(unitC.getString());
		ret.append(unitI.getString());
		
		return ret.toString();
    }
    
    /**
     */
    @Override
    public String toString() { return ", del :"+deleted+", "+toStringInternal(); }
 
 	/**
 	 */
    private String toStringInternal() {
		return "["+getRaci()+"], userId : "+getPrimaryUserId();
    }

	/**
	 */
	@Override
    public int hashCode(){ return toStringInternal().hashCode(); }

    /**
	 * Returns true if the two RACI are identical (same user & object & raci).
	 */
	@Override
    public boolean equals(Object o) {
		if (o == null)
			return false;
		
		if (this == o)
			return true;
		
		if (!(o instanceof RACI))
			return false;
		
		RACI ref = (RACI) o;
		return (ref.isR() == isR() && ref.isA() == isA() && ref.isC() == isC() && ref.isI() == isI() && 
				ref.getPrimaryUserId() == primaryUserId);
    }
	
	/**
	 */
	@Override
	public int compareTo(RACI other) {

		if (other == null)
			return -1;
		
		if (other.getBestRank() < getBestRank())
			return -1;
		
		if (other.getBestRank() == getBestRank())
			return 0;
		
		return 1;
	}
}
