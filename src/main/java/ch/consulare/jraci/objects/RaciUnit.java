/* 
  jRACI projects, https://sourceforge.net/p/jraci/
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.objects;

/**
 * Defines the basic R A C I atomic enum, a R or A or C or I unit and a state (true or false).
 */
public enum RaciUnit {
	NULL 	(null, false),
	R		(R_A_C_I.R, true),
	R_FALSE	(R_A_C_I.R, false),
	A		(R_A_C_I.A, true),
	A_FALSE	(R_A_C_I.A, false),
	C		(R_A_C_I.C,	true),
	C_FALSE	(R_A_C_I.C, false),
	I		(R_A_C_I.I, true),
	I_FALSE	(R_A_C_I.I, false);

	private R_A_C_I	raciUnit;
	private boolean state;

	/* Enum constructor**/
	RaciUnit(final R_A_C_I _raciUnit, final boolean _state) { 
		raciUnit	= _raciUnit;
		state		= _state;
	}
	
	/*
	 * Construct the RaciUnit matching the R_A_C_I and associated state.
	 * Returns null for R_A_C_I.NULL and will throw exception if the String is not a R_A_C_I.
	**/
	public static RaciUnit getRaciUnit(String stringRaci, boolean state) {
		final R_A_C_I raci = R_A_C_I.getValueOf(stringRaci);
		if (raci == null) return null;
		
		switch (raci) {
			case R	: return (state ? R : R_FALSE);
			case A	: return (state ? A : A_FALSE); 
			case C	: return (state ? C : C_FALSE); 
			case I	: return (state ? I : I_FALSE);
			default	: break;
		}

		return null;
	}

	/*
	 * Construct the RaciUnit matching the R_A_C_I and associated state.
	 * Returns null if the the R_A_C_I is NULL. 
	**/
	public static RaciUnit getRaciUnit(R_A_C_I raci, boolean state) {
		if (raci ==  null) return null;
		switch (raci) {
			case R	: return (state ? R : R_FALSE);
			case A	: return (state ? A : A_FALSE);
			case C	: return (state ? C : C_FALSE);
			case I	: return (state ? I : I_FALSE);
			default	: break;
		}

		return null;
	}

	/** Returns the R A C I state */
	public boolean isState() { return state; }

	/** Returns the unit */
	public R_A_C_I getUnit() { return raciUnit; }
	
	/** Returns a String representation of the object */
	public String toString() { return "["+raciUnit+":"+Boolean.toString(state)+"]"; }

	/** Returns the letter if set otherwise empty string */
	public String getString() { return (state ? raciUnit.toString() : ""); }

	/** Returns true if the Unit is R  */
	public boolean isR()		{ return ( raciUnit == R_A_C_I.R); }
	/** Returns true if the Unit is A */
	public boolean isA()		{ return ( raciUnit == R_A_C_I.A); }
	/** Returns true if the Unit is C  */
	public boolean isC()		{ return ( raciUnit == R_A_C_I.C); }
	/** Returns true if the Unit is I */
	public boolean isI()		{ return ( raciUnit == R_A_C_I.I); } 
	/** Returns true if the Unit is null (true) */
	public boolean isNull()	{ return ( raciUnit == null || raciUnit == raciUnit.NULL); }
}
