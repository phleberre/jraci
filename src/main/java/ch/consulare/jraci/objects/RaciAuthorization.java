/* 
  jRACI project
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.objects;

/**
 * Objects to holds the results of a RACI analysis by the rule engine. If undecided then
 * both denied and granted are false.
 */
public final class RaciAuthorization implements java.io.Serializable {
	static final long serialVersionUID = -8596258203514278330L;

	private String	raciOperation	= null;		// For bean/soap makes string for now, need a deserializer for RaciOperation
	private	boolean granted			= false;	// Rules engine says ok
	private	boolean	denied			= false;	// Rules engine says
	private	Integer	raciObjectId	= null;		// Raci Object Id
	
	public RaciAuthorization()  { /* nada */ }
	
	public RaciAuthorization(String _raciOperation, Integer _raciObjectId) {
		raciOperation = _raciOperation;
		raciObjectId = _raciObjectId;
	}
	
	public void		setGranted(boolean _granted) { granted = _granted; denied = ! granted; }
	public boolean	getGranted() { return granted; }
	
	public void	setDenied(boolean _denied) { denied = _denied; granted = ! denied; }
	public boolean getDenied() { return denied; }
		
	public void setRaciOperation(String _raciOperation) { raciOperation  = _raciOperation; }
	public String getRaciOperation() { return raciOperation; }
	
	public void setRaciObjectId(Integer _raciObjectId) { raciObjectId  = _raciObjectId; }
	public Integer getRaciObjectId() { return raciObjectId; }
	
	@Override
	public String toString() { return "{ op: "+raciOperation+", granted : "+granted+", denied : "+denied+", raciObjectId :" + raciObjectId+ "}"; }
}
