/* 
  jRACI project
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.objects;

import java.util.Locale;

/**
 * Defines the basic enum of a R A C I 
 */
public enum R_A_C_I {
	NULL	("NULL", -1),		// default value for serializer
	R		("R", 2),	// Responsible
	A		("A", 1),	// Accountable is greater than Responsible
	C		("C", 3),	// Consulted
	I		("I", 4);	// Informed

	private int			rank;		// order (1 is higher then 4)
	private String		letter;

	R_A_C_I(final String _letter, final int _rank) { 
		rank	= _rank;
		letter	= _letter;
	}
	
	/**
	 * Returns the rank (A is 1 and I is 4).
	*/
	public int getRank() { return rank; }

	/**
	 * Returns the associated letter. I know the enum can
	 * do this, it's just in case we'd like to have something
	 * fancy.
	 */
	public String getLetter() { return letter; }

	/**
	 * Returns the associated letter in lower case
	 */
	public String getLowerCaseLetter() { return letter.toLowerCase(Locale.US); }
	
	/**
	 * Returns true if the String is a raci letter (R,A,C,I), not case sensitive
	 */
	public static boolean isRACI(final String string) {
		final R_A_C_I raci = R_A_C_I.getValueOf(string);
		return (raci != null && raci != NULL);
	}
	
	/**
	 * Returns true if the String is a raci letter (R,A,C,I), not case sensitive
	 */
	public static R_A_C_I getValueOf(final String string) {
		for (final R_A_C_I v : R_A_C_I.values()) {
			if (v.toString().equalsIgnoreCase(string))
				return (v == NULL ? null : v);
		}
		
		return null;
	}
}
