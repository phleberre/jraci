/* 
  jRACI project
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.objects;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.stream.Collectors;

import ch.consulare.jraci.IRaciMatrix;
import ch.consulare.jraci.IRaciObjectType;
import ch.consulare.jraci.IRaciOperation;
import ch.consulare.jraci.RaciException;

/**
 */
public class RaciMatrix implements IRaciMatrix, java.io.Serializable {
	private static final long serialVersionUID = -8518495788837573587L;

	private List<RACI>		matrix 			= null; 		// RACI matrix
	private	String			raciObjectId	= null;			// The unique id of the objects to which the RACI belongs
	private	IRaciObjectType	raciObjectType	= null; 		// The type of RACI
	private RaciId			raciId			= null;			// RACI id with serial
	private boolean			readOnly		= false;		// locked RACI
	
	/**
		TODO:
		- DONE - CHECK THAT RACI line are complete, meaningful before adding.
		- DONE - locked, one way flag to ensure the RACI is never modified
		- tabbed String display
		- where do we want to track changes, at RACI line level or matrix level? See RaciDiff
		- logger / log4j - or we will leave that to consumers ?
		
		out of scope
			- JSON output/input (a factory Class or serializier) - jackson 2 https://github.com/FasterXML/jackson - or we let everyone do it their own?
			- Shouldn't "can" be outside. RaciAuthorization ? Decoupled the logic/meaning from the data	

	*/
	
	/** Default constructor */
	public RaciMatrix() { /* zilch */ }
	
	/** */
	public RaciMatrix(final RaciId _raciId, final String _raciObjectId, 
						final IRaciObjectType _raciObjectType) 
							throws IllegalArgumentException, IllegalStateException, RaciException {
		setRaciObjectId(_raciObjectId);
		setRaciId(_raciId);
		setRaciObjectType(_raciObjectType);
	}
	
	/** Pre feed the matrix*/
	public RaciMatrix(final RaciId _raciId, final String _raciObjectId, 
						final IRaciObjectType _raciObjectType, final List<RACI> _matrix) 
							throws IllegalArgumentException, IllegalStateException, RaciException {
		this(_raciId, _raciObjectId, _raciObjectType);
		if (_matrix == null)
			throw new IllegalArgumentException("The RACI matrix cannot be null");
		setMatrix(_matrix);
	}

	/** Creates a read only matrix */
	public RaciMatrix(final RaciId _raciId, final String _raciObjectId, 
						final IRaciObjectType _raciObjectType, final boolean _readOnly, final List<RACI> _matrix) 
							throws IllegalArgumentException, IllegalStateException, RaciException  {
		this(_raciId, _raciObjectId, _raciObjectType, _matrix);
		// lock it
		readOnly = _readOnly;
	}
	
	/** Returns the size of the matrix */
	public int size() {
		return (matrix == null ? 0 : matrix.size());
	}
	
	/** Returns read-only setting */
	public boolean isReadOnly() { return readOnly; }
	 
	/** Returns the full RACI matrix */
	public List<RACI> getMatrix() { return matrix; }

	/** is Readonly */
	public void checkReadOnly() throws RaciException {
	  	if (readOnly)
			throw new RaciException("This matrix ("+raciId+") is read-only access");
	}
		
	/** internal */
	private void checkNullMatrix() throws IllegalStateException {
		if (matrix == null)
			throw new IllegalStateException("The RACI matrix ("+raciId+") is null");
	} 
	
	/** 
	 * Adds a (non null && unique) line to the matrix. 
	 * A user can only be present once in a matrix, so even if there's a line flagged as deleted with the user, it will be rejected.
	 * A line must be complete, so have at least one level set
	 * A null line cannot be added
	 * A duplicate obviously cannot be added
	 */
	public void addRACI(final RACI value) throws IllegalArgumentException, RaciException {
		checkReadOnly();
			
		if (value == null)
			throw new IllegalArgumentException("Cannot add a null RACI to matrix ("+raciId+")");
		
		if (matrix == null)
			matrix = new ArrayList<RACI>();
		
		// User set ?
		if (value.getPrimaryUserId() == null)
			throw new RaciException("Cannot add a RACI with a null PrimaryUserId in matrix ("+raciId+")");
			
		// already a line for this user?
		if (isRACI(value.getPrimaryUserId()))
			throw new RaciException("There is already a RACI for this user ("+value.getPrimaryUserId()+") in matrix ("+raciId+")");
		
		// is the RACI ok?
		if (! value.isAny())
			throw new RaciException("Cannot add a non-set RACI to matrix ("+raciId+"), at least one R,A,C,I must be set");
				
		// no duplicates, by now that shouldn't happen but oh well, paranoid
		// The deleted flag is not used in the comparison, so adding again a deleted line will work
		if (! matrix.contains(value))
			matrix.add(value);		
	}
	
	/** 
	* Adds a List of RACIs, @see addRACI for restrictions
	*/
	public void addRACI(final List<RACI> values) throws IllegalArgumentException, RaciException {
		if (values == null)
			throw new IllegalArgumentException("Cannot add a null List<RACI> to matrix ("+raciId+")");

		for(final RACI value : values)
			addRACI(value);	
	}
	
	/** Sets the matrix */
	public void setMatrix(final List<RACI> values) throws IllegalArgumentException, IllegalStateException, RaciException {
		if (values == null)
			throw new IllegalArgumentException("Cannot set a matrix ("+raciId+") to null");	
		
		if (matrix != null)
			throw new IllegalStateException("The matrix ("+raciId+") is already set");
			
		matrix = new ArrayList<RACI>();
		addRACI(values);		
	}
	
	/** Flags as deleted a line of the matrix */
	public void deleteRACI(final RACI value) throws IllegalArgumentException, RaciException {
		checkReadOnly();
		checkNullMatrix();

		if (value == null)
			throw new IllegalArgumentException("Cannot delete null RACI in matrix ("+raciId+")");
						
		for(final RACI raci : matrix) {
			if (raci.equals(value)) {
				raci.setDeleted(true);
			}
		}
	}

	/** Removes all deleted lines from the matrix */
	public void purgeMatrix() throws RaciException {
		checkReadOnly();

		int count = 0;
		if (matrix != null) {
			for(Iterator<RACI> iter = matrix.listIterator(); iter.hasNext();) {
				final RACI raci = iter.next();
				if (raci.isDeleted()) {
					iter.remove();
					++count;
				}
			}
		}
		//_logger.debug("Deleted ("+count+") lines from RACI")
	}
	
	/** 
	* Updates a RACI, is this a delete then remove or use RaciDiff with history
	* This method relies on the RACI internal UUID for the update.
	*/
	public void updateRACI(final RACI value) throws IllegalArgumentException, IllegalStateException, RaciException {
		checkReadOnly();
		checkNullMatrix();

		if (value == null)
			throw new IllegalArgumentException("Cannot delete null RACI in matrix ("+raciId+")");

		// get the RACI
		final List<RACI> origRACI = matrix.stream().filter(r -> r.getRaciUUID().equals(value.getRaciUUID())).collect(Collectors.toList());
		
		// checks
		if (origRACI == null || origRACI.size() == 0)
			throw new RaciException("Cannot find the RACI with UUID ("+value.getRaciUUID()+") in matrix ("+raciId+")");
		
		if (origRACI.size() > 1)
			throw new IllegalStateException("The RACI matrix ("+raciId+") has ("+origRACI.size()+") lines with the same UUID ("+value.getRaciUUID()+")");
			
		// update
	}
	
	/** Sets the RaciId of the matrix */
	public void setRaciId(final RaciId value) throws IllegalArgumentException, IllegalStateException, RaciException {
		checkReadOnly(); // paranoid

		if (value == null)
			throw new IllegalArgumentException("Cannot set a matrix with a null RaciId");
			
		if (raciId != null)
			throw new IllegalStateException("The raciId of matrix ("+raciId+") is already set");
	
		raciId = value;
	}
	
	/** Returns the RaciId of the matrix */
	public RaciId getRaciId() { return raciId; }
	
	/** Sets the RaciObjectType, which cannot be changed afterwards, immutable */
    public void setRaciObjectType(IRaciObjectType _raciObjectType) throws IllegalStateException, RaciException { 
 		checkReadOnly();

	 	if (raciObjectType != null)
			throw new IllegalStateException("The RaciObjectType of matrix ("+raciId+") is already set");
 		
    	raciObjectType = _raciObjectType; 
    }
    
    /** Returns the RaciObjectType */
    public IRaciObjectType getRaciObjectType() { return raciObjectType; }

	/** Sets the RaciObjectId, which cannot be changed afterwards, immutable */	
	public void setRaciObjectId(final String _raciObjectId) throws IllegalStateException, RaciException { 
		checkReadOnly();

		if (raciObjectId != null)
			throw new IllegalStateException("The RaciObjectId of matrix ("+raciId+") is already set");

		raciObjectId = _raciObjectId; 
	}
    
    /** Returns the RaciObjectId */
    public String getRaciObjectId() { return raciObjectId; }
	
	/** associate RACI to Operations. Should this be here ? or just abstract ? */
	@Override
	public boolean can(final String primaryUserId, final IRaciOperation raciOperation) {
		return false;
	}

	/** 
	 * Returns the list of RACI line(s) applying to specific userId. In usual situation there should be just a single
	 * line, but lines flagged as deleted will also be returned.
	 *
	 * @return null or the list of RACI matching the primary user id
	 * @param primaryUserId the id of the primary user
	 * @deleted if true includes the RACI flagged as deleted
	*/
	public List<RACI> getRaciForPrimaryUser(final String primaryUserId, final boolean deleted) throws IllegalArgumentException {
		if (primaryUserId == null)
			throw new IllegalArgumentException("The primaryUserId cannot be null");
			
		if (matrix == null) return null;
		
		return matrix.stream().filter(r -> r.getPrimaryUserId().equals(primaryUserId) && (deleted ? r.isDeleted() : !r.isDeleted())).collect(Collectors.toList());
	}
	
	/**
	 * Returns the list of RACI line(s) matching the selected RaciUnit.
	 *
	 * @return null or the list of RACI matching the RaciUnit
	 * @param unit the selected RaciUnit
	 * @deleted if true includes the RACI flagged as deleted
	*/
	public List<RACI> getRaci(final RaciUnit unit, final boolean deleted) throws IllegalArgumentException {
		if (unit == null)
			throw new IllegalArgumentException("The unit cannot be null");
			
		if (matrix == null) return null;
		
		return matrix.stream().filter(r -> r.contains(unit) && (deleted ? r.isDeleted() : !r.isDeleted())).collect(Collectors.toList());
	}
	
	
	/**
	 * Returns true if the primaryUserIs is present in the RACI matrix
	 */
	public boolean isRACI(final String primaryUserId) {
		final List<RACI> lines = getRaciForPrimaryUser(primaryUserId, false);
		return (lines != null && lines.size() > 0);
	}
	
	/**
	 * Returns true if the primaryUserId is present at the requested level in the RACI matrix. No using deleted RACI.
	 */
	 public boolean isRACI(final String primaryUserId, final RaciUnit raci) {
		final List<RACI> lines = getRaciForPrimaryUser(primaryUserId, false);
		if (lines != null) {
			for(final RACI line : matrix) {
				if (line.equals(raci) && !line.isDeleted()) 
					return true;	
			}
		}
		
		return false;
	 }
	
	/** Returns true if the primaryuserId is R in the RACI matrix */
	public boolean isR(final String primaryUserId) {
		return isRACI(primaryUserId, RaciUnit.R);
	}

	/** Returns true if the primaryuserId is A in the RACI matrix */
	public boolean isA(final String primaryUserId) {
		return isRACI(primaryUserId, RaciUnit.A);
	}
	
	/** Returns true if the primaryuserId is C in the RACI matrix */
	public boolean isC(final String primaryUserId) {
		return isRACI(primaryUserId, RaciUnit.C);
	}
	
	/** Returns true if the primaryuserId is I in the RACI matrix */
	public boolean isI(final String primaryUserId) {
		return isRACI(primaryUserId, RaciUnit.I);
	}		
	
	/** Returns true if the primaryuserId is RA in the RACI matrix */
	public boolean isRA(final String primaryUserId) {
		return (isRACI(primaryUserId, RaciUnit.R) && isRACI(primaryUserId, RaciUnit.A));
	}		
}