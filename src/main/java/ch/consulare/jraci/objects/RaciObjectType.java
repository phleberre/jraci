/* 
  jRACI projects, https://sourceforge.net/p/jraci/
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.objects;

import java.util.Locale;
import ch.consulare.jraci.IRaciObjectType;

/**
 * Raci Object type. Simple bean to handle them.
*/
public final class RaciObjectType implements IRaciObjectType, java.io.Serializable {
	private static final long serialVersionUID = 2048755762565014540L;

	private String 			typeId = null;
	private String			typeName = null;
	private String			domain = null;
	
	/**
	 Default constructor
	 */
	public RaciObjectType() { }
	
	/**
	 * Creates a RaciObjectType.
	 *
	 * @param _typeId a integer greater than 1, that should be unique in its namespace
	 * @param _typeName a non null String, that should be unique in its namespace
	 */
	public RaciObjectType(final String _typeId, final String _typeName) throws IllegalArgumentException {
		if (_typeId == null)
			throw new IllegalArgumentException("The typeId cannot be null");
		if (_typeName == null)
			throw new IllegalArgumentException("The typeName cannot be null");
			 
		typeId		= _typeId; 
		typeName	= _typeName;
	}
	
	/**
	 * Returns the unique int id 
	*/
	public String getTypeId() { return typeId; }
	public void setTypeId(final String value) throws IllegalArgumentException { 
		if (value == null)
			throw new IllegalArgumentException("The typeId cannot be null");	
		typeId = value; 
	}
	
	/** Returns the unique type name */
	public String getTypeName() { return typeName; }
	
	/** Sets the typeName which cannot be null */
	public void setTypeName(final String value) throws IllegalArgumentException { 
		if (value == null)
			throw new IllegalArgumentException("The name of a RACIObjectType cannot be null");
		typeName = value; 
	}
	
	/** Returns the domain */
	public String getDomain() { return domain; }

	/** Sets the domain */
	public void setDomain(final String value) throws IllegalArgumentException { 
		domain = value; 
	}
	
	/**
	 * Returns the typeName in lowercase
	 */
	@Override
	public String toString() { return (typeName == null ? "null" : typeName.toLowerCase(Locale.US)); }
}
