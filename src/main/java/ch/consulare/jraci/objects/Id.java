/* 
  jRACI project
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */


package ch.consulare.jraci.objects;

/**
 * Subclass this, do not use it directly.
 */
public abstract class Id implements java.io.Serializable {
 	static final long serialVersionUID = 972522662810411331L;
 
    private int id;
    private int serial;
	
	/** Basic constructor */
    public Id() {
		this.id		=	0;
		this.serial	=	0;
    }
    
    /** */
    public Id(final int id, final int objectSerial) throws IllegalArgumentException {
		if (id == 0) throw new IllegalArgumentException("id is 0"); // cannot return null
		this.id = id;
		this.serial = objectSerial;
    }
	
	/** True if set (means no 0) */
	public final boolean isSet() {
		return (this.id > 0L);
	}

    public final boolean checkSerial(final Id other) {
		if (other == null) 
			throw new IllegalArgumentException("Object must not be null.");
		if (this.getClass() != other.getClass())
			throw new IllegalArgumentException("Comparing two different objects ("+getClass().getName()+") vs. ("+other.getClass().getName()+")");
		if (id != other.id) 
			throw new IllegalArgumentException("Comparing two different objects ("+id+" vs. "+other.id+")");
		
		if (serial == 0L || other.serial == 0L) return true;

		return serial == other.serial;
    }

    public final int getId() { return id; }
    public final int getSerial() { return serial; }

    public final void setId(final int id) { this.id = id; }
    public final void setSerial(final int objectSerial) { this.serial = objectSerial; }

	@Override
    public String toString() {
		final StringBuffer sb = new StringBuffer();
		sb.append("id=").append(id).append(", serial=").append(serial);
		return sb.toString();
    }

    /* this is required for equals */
    @Override
    public int hashCode() { return id; }

    // this is required to make sure that two vuln ids relate to the same vulnerability,
    // for this we don't actually care at all about their serial numbers.
    @Override
    public boolean equals(Object other) {
		if (other == null)
		    return false;
		if (this.getClass() != other.getClass())
		    return false;
		return id == ((Id)other).id;
    }
}
