/* 
  jRACI project
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.objects;

import java.util.TimeZone;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.Locale;

import ch.consulare.jraci.RaciException;

/**
 * A single history record for a RACI
*/
public class RaciHistory extends RaciDiff {
	private static final long serialVersionUID = -3092363356891865241L;
		
	// Default UTC now
	private Date	modificationDate = new GregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US).getTime();
	
	private String 	modifierPrimaryUserId = null;

	/** 
	 * Default constructor 
	 *
	 * @param origin the original RACI, cannot be null
	 * @param modified the modified RACI, cannot be null
	 */
	public RaciHistory(final RACI origin, final RACI modified) throws RaciException, IllegalStateException, IllegalArgumentException {
		super(origin, modified);
	}
	
	/** Sets the primary id of the modifier, which can be null */
	public void setModifierPrimaryUserId(final String primaryUserId) {
		modifierPrimaryUserId = primaryUserId;
	}
	
	/** Returns the primary user id of the modifier */
	public String getModifierPrimaryUserId() { return modifierPrimaryUserId; }
	
	/** Sets the time when the change occur */
	public void setModificationDate(final Date value) { modificationDate = value; }
	
	/** Get the modification date. The default one is the object instantiation time in UTC timezone. */
	public Date getModificationDate() { return modificationDate; }	
}