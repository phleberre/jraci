/* 
  jRACI project
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.util;

import java.util.List;

/** 
 * Helper class
 */
public final class StringHelper {

	/** Singleton */
	private StringHelper() { /* singleton*/ }

	/**
	 * nullify - turns the empty "" string into a null object and trims any whitespace. If only to check prefer the prefer the isEmpty method.
	 */
	public static String nullify(final String s) {
		if (s == null) {
			return null;
		} else {
			// trim \u0000 to avoid ERROR: invalid byte sequence for encoding "UTF8": 0x00
			final String str = s.replaceAll("\0", "").trim();
			if ("".equals(str))
				return null;
			
			return str;
		}
	}

	/**
	 * unnullify - turns a null string into an empty string
	 */
	public static String unnullify(final String s) {
		return (s == null ? "" : s);
	}
	
   /**
     * Joins an array with fixed with for each field using a supplied join 
     * separator and a supplied padding.
     */ 
    public static String joinFixedWidth(final String in[], final int length[], final String sepIn, final String padIn) {
		if (in == null || length == null) return null;
		if (in.length == 0) return null;

		final StringBuffer output = new StringBuffer();
		final String sep = unnullify(sepIn);
		final String pad = (padIn == null ? " " : padIn);

		for(int i=0; i<in.length; ++i) {
		    if (i>0) output.append(sep);
		    output.append(unnullify(in[i]));
		    for (int j= unnullify(in[i]).length(); j<length[i]; ++j)
				output.append(pad);
		}
	    
		return output.toString();
	}
	
	/*
	 * Reduce boiler plate on display
	 */
	 public final static String getOutTable(final String[] title, String[] spacer, int[] maxlength, final List<String[]> output) {
	 	final StringBuffer sb = new StringBuffer("\n");
	 	
	 	// sanity
	 	if (title == null)
	 		throw new IllegalArgumentException("The title cannot be null");
	 		
	 	if (output != null && ! output.isEmpty() && output.get(0) != null && title.length != output.get(0).length)	
	 		throw new IllegalArgumentException("The title length("+title.length+") mismatch output record length ("+output.get(0).length+")");
	 		
	 	// build spacer
	 	if (spacer == null) {
	 		spacer = new String[title.length];
	 		for (int i=0; i < spacer.length; ++i) { spacer[i] = "-"; }
	 	}
	 	
	 	// build maxlength
	 	if (maxlength == null) {
			maxlength = new int[title.length];
			for (int i=0; i < spacer.length; ++i) {
				maxlength[i] = (title[i].length() > maxlength[i] ? title[i].length() : maxlength[i]);
				for (String[] s : output) {
					maxlength[i] = (s[i] == null ? maxlength[i] : (s[i].length() > maxlength[i] ? s[i].length() : maxlength[i]));
				}
			}
	 	}
	 	
	 	sb.append(joinFixedWidth(title, maxlength, " | ", null)).append("\n");
		sb.append(joinFixedWidth(spacer, maxlength, "-+-", "-")).append("\n");
		for (int i=0; i<output.size(); ++i) { 
		    sb.append(joinFixedWidth(output.get(i), maxlength, " | ", null)).append("\n");
		}
		sb.append(joinFixedWidth(spacer, maxlength, "-+-", "-")).append("\n");
		
		return sb.toString();
	 }
}