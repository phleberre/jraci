/* 
  jRACI projects, https://sourceforge.net/p/jraci/
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.test;

import java.util.List;
import java.util.ArrayList;
import java.util.EnumSet;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import ch.consulare.jraci.objects.R_A_C_I;
import ch.consulare.jraci.objects.RaciUnit;

/**
 * Unit tests for CVE Id object
 */
public class Test00RaciEnums  {

	@Test
	public void test_R_A_C_I_enum_values() {
		List<R_A_C_I> list = new ArrayList<R_A_C_I>(EnumSet.allOf(R_A_C_I.class));
		for (R_A_C_I i : list) {
			if (i == R_A_C_I.NULL) {
				assertFalse(R_A_C_I.isRACI(i.toString())); 
			} else {
				assertTrue(R_A_C_I.isRACI(i.toString()));
			}
		}
	}

	@Test
	public void test_R_A_C_I_enum_values_lowercase() {		
		assertTrue(R_A_C_I.isRACI("a"));
	}
	
	@Test
	public void test_R_A_C_I_bad_enum_values() {
		assertFalse(R_A_C_I.isRACI("Foo"));
		assertFalse(R_A_C_I.isRACI(""));
		assertFalse(R_A_C_I.isRACI(null));
	}

	@Test
	public void test_R_A_C_I_getValueOf() {
		assertNull(R_A_C_I.getValueOf(null));
		assertNull(R_A_C_I.getValueOf("NULL"));
		assertNull(R_A_C_I.getValueOf("foo"));
		assertEquals(R_A_C_I.A, R_A_C_I.getValueOf("a"));
		assertEquals(R_A_C_I.C, R_A_C_I.getValueOf("C"));
	}
		
	@Test
	public void test_RaciUnit_getRaci_with_bad_values() {
		assertNull(RaciUnit.getRaciUnit((String) null, false));
		assertNull(RaciUnit.getRaciUnit("", false));
		assertNull(RaciUnit.getRaciUnit("foo", false));
		assertNull(RaciUnit.getRaciUnit((R_A_C_I) null, false));
		assertNull(RaciUnit.getRaciUnit(R_A_C_I.NULL, false));
	}
	
	@Test
	public void test_RaciUnit_getRaci_with_ok_values() {
		assertEquals(RaciUnit.A_FALSE, RaciUnit.getRaciUnit("A", false));
		assertEquals(RaciUnit.A, RaciUnit.getRaciUnit("A", true));
		assertEquals(RaciUnit.R, RaciUnit.getRaciUnit("r", true));
		assertEquals(RaciUnit.R, RaciUnit.getRaciUnit(R_A_C_I.R, true));
		assertEquals(RaciUnit.A_FALSE, RaciUnit.getRaciUnit(R_A_C_I.A, false));
	}
	
	@Test
	public void test_RaciUnit_is_x() {
		assertTrue(RaciUnit.getRaciUnit("a", true).isA());
		assertFalse(RaciUnit.getRaciUnit("R", true).isA());
		assertTrue(RaciUnit.getRaciUnit("A", false).isA());
		assertTrue(RaciUnit.getRaciUnit("R", true).isR());
		assertTrue(RaciUnit.getRaciUnit("C", true).isC());
		assertTrue(RaciUnit.getRaciUnit("I", true).isI());
	}

}