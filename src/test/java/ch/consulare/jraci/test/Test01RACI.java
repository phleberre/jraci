/* 
  jRACI projects
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import ch.consulare.jraci.objects.RACI;
import ch.consulare.jraci.objects.RaciUnit;
import ch.consulare.jraci.objects.R_A_C_I;

public class Test01RACI  {
	
	@Test
	public void test01_equal() {
		RACI r1 = new RACI();
		RACI r2 = new RACI();

		r1.setR(true);
		r1.setI(true);
		r1.setPrimaryUserId("1000");
		
		r2.setR(true);
		r2.setI(true);
		r2.setPrimaryUserId("1000");
		
		assertEquals(r1, r2);
		assertEquals(r1, r1);
		
		assertEquals(r1.hashCode(), r2.hashCode());
	}
	
	@Test
	public void test02_equal() {
		RACI r1 = new RACI();
		
		r1.setR(true);
		r1.setI(true);
		r1.setPrimaryUserId("1000");
		
		RACI r2 = new RACI(r1);

		assertEquals(r1, r2);
	}
	
	@Test
	public void test03_not_equal() {
		RACI r1 = new RACI();
		RACI r2 = new RACI();
		
		r1.setR(true);
		r1.setI(true);
		r1.setPrimaryUserId("1000");
		
		r2.setA(true);
		r2.setI(true);
		r2.setPrimaryUserId("1000");
		
		assertFalse(r1.equals(r2));
	}
	
	@Test
	public void test04_isRA() {
		RACI r = new RACI();
		
		r.setR(true);
		r.setA(false);
		assertTrue(r.isRA());
		
		r.setR(false);
		r.setA(true);
		assertTrue(r.isRA());

		r.setA(false);
		r.setR(false);
		assertFalse(r.isRA());
	}
		
	@Test
	public void test05_isNotValid() throws Exception {
		RACI r = new RACI();

		r.setI(true);
		r.setA(true);
		assertFalse(r.isValid());
		
		r.reset();
		
		r.setC(true);
		r.setR(true);
		assertFalse(r.isValid());
		
		r.reset();
		
		r.setC(true);
		r.setR(true);
		r.setA(true);
		assertFalse(r.isValid());
		
		r.reset();
		
		r.setC(true);
		r.setR(true);
		r.setA(true);
		r.setI(true);
		assertFalse(r.isValid());
	}
	
	@Test
	public void test06_isValid() {
		RACI r = new RACI();
		assertTrue(r.isValid());
		
		r.setC(true);
		assertTrue(r.isValid());		
		r.reset();
		r.setI(true);
		assertTrue(r.isValid());
		r.reset();
		r.setA(true);
		assertTrue(r.isValid());
		r.reset();
		r.setC(true);
		assertTrue(r.isValid());
		r.reset();
		r.setA(true);
		r.setR(true);
		assertTrue(r.isValid());
	}

	@Test
	public void test07_contains() {
		RACI r = new RACI(RaciUnit.R, "0");
		assertTrue(r.contains(RaciUnit.R));
		assertFalse(r.contains(RaciUnit.A));
	}
					
	@Test
	public void test08_isRA_RaciUnit() throws Exception {
		// Just A
		RACI r = new RACI(RaciUnit.A, "0");
		assertTrue(r.isRA());
			
		// R && A
		r.setUnitR(RaciUnit.R);	
		assertTrue(r.isRA());

		// None
		r.reset();
		assertFalse(r.isRA());
	}
	
	@Test
	public void test09_setRaciUnit_bad() throws Exception {
		assertThrows(IllegalArgumentException.class, () -> {
			RACI r = new RACI(RaciUnit.A, "0");
			r.setUnitC(RaciUnit.I);
		});
	}
	
	@Test
	public void test10_setRaciUnit() throws Exception {
		RACI r = new RACI(RaciUnit.I, "0");
		r.setUnitC(RaciUnit.C);
		assertTrue(r.isC());
	}
	
	@Test
	public void test11_getRaci() throws Exception {
		RACI r = new RACI(RaciUnit.A, "0");
		assertEquals("A", r.getRaci());
		
		r.setUnitC(RaciUnit.C);
		assertEquals("AC", r.getRaci());
	}
	
	@Test
	public void test12_isCI() throws Exception {
		RACI r = new RACI(RaciUnit.A, "0");
		assertFalse(r.isCI());
		
		r.setUnitA(RaciUnit.A_FALSE);
		r.setUnitC(RaciUnit.C);
		assertTrue(r.isCI());
		
		r.setUnitI(RaciUnit.I);
		assertTrue(r.isCI());
	}
	
	@Test
	public void test13_compare() throws Exception {
		RACI r1 = new RACI(RaciUnit.A, "0");
		RACI r2 = new RACI(RaciUnit.I, "0");

		assertTrue(r1.compareTo(r2) > 0);
		assertTrue(r2.compareTo(r1) < 0);
		assertEquals(r2.compareTo(r2), 0);
	}
	
	@Test
	public void test14_get_best_rank() throws Exception {
		RACI r = new RACI();
		r.setI(true);
		r.setA(true);
		assertEquals(R_A_C_I.A.getRank(), r.getBestRank());
	}
}