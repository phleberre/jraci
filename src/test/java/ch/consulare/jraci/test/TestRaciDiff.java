/* 
  jRACI projects
   
  Redistribution and use in source and binary forms, with or without modification, are permitted provided 
  that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following 
     disclaimer in the  documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
  The views and conclusions contained in the software and documentation are those of the authors and 
  should not be interpreted as representing official policies, either expressed or implied, of the project stakeholders.
 */

package ch.consulare.jraci.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import ch.consulare.jraci.objects.RACI;
import ch.consulare.jraci.objects.RaciState;
import ch.consulare.jraci.objects.RaciHistory;
import ch.consulare.jraci.RaciException;

public class TestRaciDiff  {

	@Test
	public void TestRaciHistoryWithSameRACI() throws RaciException {
		RACI raciA = new RACI("QWE", true, false, false, true);
		RaciHistory rh = new RaciHistory(raciA, raciA);
		assertEquals(0, rh.getNumberOfChanges());
		assertFalse(rh.hasChanged());
		assertEquals(rh.getR(), RaciState.SET);
		assertEquals(rh.getA(), RaciState.NOTSET);
		assertEquals(rh.getC(), RaciState.NOTSET);
		assertEquals(rh.getI(), RaciState.SET);
	}

	@Test
	public void TestRaciHistoryWithNoSetSameRACI() throws RaciException {
		RACI raciA = new RACI("QWE", false, false, false, false);
		RaciHistory rh = new RaciHistory(raciA, raciA);
		assertEquals(0, rh.getNumberOfChanges());
		assertFalse(rh.hasChanged());
		assertEquals(rh.getR(), RaciState.NOTSET);
		assertEquals(rh.getA(), RaciState.NOTSET);
		assertEquals(rh.getC(), RaciState.NOTSET);
		assertEquals(rh.getI(), RaciState.NOTSET);
	}

	public void TestRaciHistoryWithAllSetSameRACI() throws RaciException {
		RACI raciA = new RACI("QWE", true, true, true, true);
		RaciHistory rh = new RaciHistory(raciA, raciA);
		assertEquals(0, rh.getNumberOfChanges());
		assertFalse(rh.hasChanged());
		assertEquals(rh.getR(), RaciState.NOTSET);
		assertEquals(rh.getA(), RaciState.NOTSET);
		assertEquals(rh.getC(), RaciState.NOTSET);
		assertEquals(rh.getI(), RaciState.NOTSET);
	}
			
	@Test
	public void TestRaciHistoryWithOneChange() throws RaciException {
		RACI raciA = new RACI("QWE", true, false, false, true);
		RACI raciB = new RACI("QWE", false, false, false, true);	
		RaciHistory rh = new RaciHistory(raciA, raciB);
		assertEquals(1, rh.getNumberOfChanges());
		assertTrue(rh.hasChanged());
		assertEquals(rh.getR(), RaciState.REMOVED);
	}
	
	@Test
	public void TestRaciHistoryWithThreeChange() throws RaciException  {
		RACI raciA = new RACI("QWE", true, false, false, true);
		RACI raciB = new RACI("QWE", false, true, false, false);	
		RaciHistory rh = new RaciHistory(raciA, raciB);
		assertEquals(3, rh.getNumberOfChanges());
		assertTrue(rh.hasChanged());
		assertEquals(rh.getR(), RaciState.REMOVED);
		assertEquals(rh.getA(), RaciState.ADDED);
		assertEquals(rh.getC(), RaciState.NOTSET);
		assertEquals(rh.getI(), RaciState.REMOVED);
	}
	
	@Test
	public void TestRaciHistoryWithFourChange() throws RaciException {
		RACI raciA = new RACI("QWE", false, true, true, false);
		RACI raciB = new RACI("QWE", true, false, false, true);	
		RaciHistory rh = new RaciHistory(raciA, raciB);
		assertEquals(4, rh.getNumberOfChanges());
		assertTrue(rh.hasChanged());
		assertEquals(rh.getR(), RaciState.ADDED);
		assertEquals(rh.getA(), RaciState.REMOVED);
		assertEquals(rh.getC(), RaciState.REMOVED);
		assertEquals(rh.getI(), RaciState.ADDED);
	}
	
	@Test
	public void TestRaciHistorySettersAndGetters() {
		//RACI raciA = 
	}
}